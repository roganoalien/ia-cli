## IA Interactive-cli
Proyecto base usando [webpack@3.8.1](https://webpack.js.org/) compilando [Stylus](http://stylus-lang.com/) y [ECMAScript](https://es.wikipedia.org/wiki/ECMAScript) con [Babel](https://babeljs.io/). 

El proyecto está pensando en hacer todo de manera modular para poder re-utilizar el código que se haga y también facilitar el mantenimiento.

Para iniciar, debemos de hacer un ```npm install``` después de clonar el proyecto para instalar todos los modulos que ocupa webpack para procesar el proyecto.

### Arreglo de Carpetas
Este **CLI** usa todos los componentes dentro de ```./src``` y dentro tiene lo siguiente:

#### Assets
```
./src/assets/
```
Dentro van las imágenes, fuentes y cualquier archivo que vaya a ser utilizado dentro del desarrollo. El único **requisito** es que las imágenes vayan en ```./src/assets/images/``` para su correcto uso y optimización.

#### Js
```
./src/js/
```
Dentro van todos los archivos de ```javascript``` del proyecto. Todo está separado para una mejor organización y sigue el principio de ```commons.js``` con la finalidad de re-utilizar código en futuros proyectos y facilitar el mantenimiento y trabajo en equipo con los mismos.

```main.js``` es usado para importar todos los archivos que serán compilados y manejados por ```webpack```. Por default sólo importa ```vendors.js``` y ```style.styl```

```index.js``` jala la información de main y es el archivo principal que jala todo lo necesario para usarse en ```index.html```. Se puede manejar un archivo js por cada html o manejar uno mismo; la mejor práctica es manejarlo por separado, al final webpack compilará todo de la mejor forma y hará la división de archivos js para evitar cargar código duplicado.

#### Stylus
```
./src/stylus/
```
Dentro van los archivos de ```stylus``` separados por carpetas:

```animations/``` lleva todas las animaciones creadas, separadas por archivos en las páginas que se van a utilizar

```config/``` todo tipo de configuración que ocupe el proyecto como variables y fuentes. Por default tiene unas variables para responsive, para nosotros poder definir de forma sencilla y rápida los tamaños que requiera el proyecto y/o estandarizar los nuestros.

```libs/``` todas las librerías externas que se ocupen y que sean **ligeras y pequeñas** para prevenir tiempos muy largos de compilado. Por default carga normalize y rupture.

```sections/``` aquí van todos los estilos que irán dentro de nuestra página, lo pueden separar por html o por pequeño modulo, para facilitar su re-utilización.

```utilities/``` todas los mixins hechos y funciones para ayudar en otros lados. Por default tiene dos mixins para utilizar en conjunto de las variables responsive de la siguiente forma:

```css
.myClass{
	background red
	+max-width($w-large)
		background green
}
```

```_general.styl``` tiene todos los estilos generales que no van sólo en las páginas pero que son más generales.

```style.styl``` aquí se importan todos los estilos y es el archivo que jala ```main.js``` para ser compilado y extraído del bundle.js.

#### Vendors
Aquí va el ```vendors.js``` que es importado en el ```main.js```

### Agregar Vendors
##### CSS
Para agregar vendors de sólo css, hay que abrir el archivo ```./src/vendors/vendors.js``` y hacer un require al module de node (node_modules). **El orden es importante al igual que primero se debe de instalar con** ```npm install -D vendor-a-instalar```. Se usa el siguiente formato para importar:

```js
require('bootstrap/dist/css/bootstrap.css');
```
##### JS
Para agregar vendors de JS, abrimos ```./webpack.config.js``` y dentro de ```module.exports = {}``` buscamos dentro de entry ```vendor: ['jquery']``` para agregar otro hacemos lo siguiente, tomando en cuenta que el orden importa y webpack primero empaqueta de derecha a izquierda:

```js
vendor: ['bootstrap', 'jquery']
```

### Agregar nuevos HTMLs
##### Dónde
Los nuevos HTML van de la siguiente forma: ```./src/nuevo.html```. No es necesario importar el css en el html ni el js, webpack lo hará por nosotros.
##### Inyectar css y js
Para poder decirle a Webpack que habrá más de un sólo html, debemos de abrir:

```js
./src/webpack.config.js
```
En caso de que vaya a tener un JS **específico** para ese html, debemos de agregar una nueva entry dentro de ```module.exports```.

```json
entry:{
	index: './js/index.js',
	minuevaentrada: './js/nuevo.js',
	vendor: ['jquery']
}
```
Es importante recordar que el nombre del archivo dependerá del nombre que en este caso representa *minuevaentrada*.

Después de indicar la nueva entrada de JS, buscamos en el mismo ```webpack.config.js``` la sección de plugins y copiamos y pegamos:

```json
new HtmlWebpackPlugin({
	chunks: ['commons', 'vendor', 'index'],
	filename: '../index.html',
	hash: true,
	inject: true,
	template: './index.html'
}),
```
Y lo pegamos justo después de ese y hacemos los cambios necesarios:

- chunks: hace referencia al JS que irá inyectado dentro del html, en el caso de index lleva commons (que es el código que se repite, siempre se debe de dejar), vendor e index, aquí reemplazamos index por el nuevo archivo js.
- filename: es el nombre que tendrá nuestro nuevo html.
- hash: incluye un hash en los archivos css y js para prevenir problemas de caché cuando se suba a producción los archivos
- inject: inyecta los archivos css y js de forma automática
- template: es el archivo que tomará como base para inyectar todos nuestros chunks.

## Comandos para desarrollo y producción
Cuando estemos en modo desarrollo es suficiente que corramos:

```js
npm run dev
```
Esto inicializará por una limpieza de la carpeta app (donde todo será compilado), montará un servidor local, compilará todo y estará haciendo un watch para cambios en los archivos. Una vez que finaliza de correr tenemos que abrir [localhost:8080](http://localhost:8080/).

Cuando vayamos a pasar a producción el proyecto o mandarlo a agregar a la app de iOS, corremos:

```js
npm run production
```
La diferencia es que ahora minifica, uglifica y gzipica css, js y html aparte de no montar un servidor local y no quedarse escuchando por nuevos cambios en los archivos.