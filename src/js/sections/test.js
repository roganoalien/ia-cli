const waitTime = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve('Han pasado 3 segundos');
    }, 3000);
});

module.exports = {
    firstMessage: 'hola mundo desde un módulo',
    delayedMessage: async() => {
        const message = await waitTime;
        console.log(message);
    }
}