const   path                        = require('path'),
        CleanWebpackPlugin          = require('clean-webpack-plugin'),
        CopyWebpackPlugin           = require('copy-webpack-plugin'),
        ExtractTextPlugin           = require('extract-text-webpack-plugin'),
        extractCSS                  = new ExtractTextPlugin('css/[name].vendor.css'),
        extractStylus               = new ExtractTextPlugin('css/[name].css'),
        HtmlWebpackPlugin           = require('html-webpack-plugin'),
        ImageminPlugin              = require('imagemin-webpack-plugin').default
        WriteFilePlugin             = require('write-file-webpack-plugin'),
        webpack                     = require('webpack');

let pathsToClean = [
    // Paths to be cleaned
    './app'
];

let cleanOptions = {
    // For settings of cleaner https://github.com/johnagan/clean-webpack-plugin
    root: __dirname,
    verbose: true,
    dry: false,
    beforeEmit: false
};

module.exports = {
    // Adds the context of where every files comes from
    context: path.resolve(__dirname, './src'),
    // entry: path.resolve(__dirname, './src/js/main.js'),
    // Add more entries if needed for different js files
    // on html files
    entry: {
        // Files that will be watched and bundled into
        index: './js/index.js',
        vendor: ['jquery']
    },
    output: {
        // Where the entry files will be build
        path: path.resolve(__dirname, 'app/public'),
        filename: 'js/[name].js',
        publicPath: 'public/',
    },
    // Dev Server Settings
    // contentBase: what folder it is served from
    // open: if it will open automatically after server is running // needs to be reloaded after ./app folder is builded
    // port: is the port the server will be listening
    devServer: {
        // for more settings https://webpack.js.org/configuration/dev-server/#devserver
        contentBase: path.resolve(__dirname, './app'),
        publicPath: 'http://localhost:8080/public/',
        // open: true,
        port: 8080
    },
    module: {
        rules: [
            {
                // Watches for .styl files loaded on main.js and builds the css files
                test: /\.styl$/,
                use: extractStylus.extract({
                    fallback: 'style-loader',
                    use: [ 
                        'css-loader', 'autoprefixer-loader?browsers=defaults', 'stylus-loader'
                    ]
                })
            },
            {
                // Watches for css loaded on main.js and concatenates them and build them as vendors
                test: /\.css$/,
                use: extractCSS.extract({
                    fallback: 'style-loader',
                    use: ['css-loader']
                })
            },
            { test: /\.html$/, loader: 'html-loader' },
            {
                // File loader for images on html
                test: /\.(jpg|jpeg|png|gif|svg)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: './assets/images/[name].[ext]'
                    }
                }
            },
            {
                // Watches JS files, except from node_modules folder and builds a bundle
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['es2015']
                    }
                }
            }
        ]
    },
    plugins: [
        extractCSS,
        extractStylus,
        // Duplicate for different HTML inputs
        new HtmlWebpackPlugin({
            chunks: ['commons', 'vendor', 'index'],
            filename: '../index.html',
            hash: true,
            inject: true,
            template: './index.html',
        }),
        // Prevents duplicate code on bundle and on calls
        new webpack.optimize.CommonsChunkPlugin({
            name: ['commons', 'vendor']
        }),
        //Deletes app folder before creating the bundle and proccessing all files
        new CleanWebpackPlugin(pathsToClean, cleanOptions),
        // Forces files to render on devserver
        new WriteFilePlugin(),
        // Copies from ./src/assets/ and changes them to ./app/public/assets
        new CopyWebpackPlugin([{
            // https://www.npmjs.com/package/copy-webpack-plugin
            from: './assets',
            to: './assets'
        }]),
        // https://github.com/Klathmon/imagemin-webpack-plugin
        new ImageminPlugin({ test: /\.(jpe?g|png|gif|svg)$/i })
    ]
}